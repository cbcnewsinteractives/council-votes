/* This is the main JS file for this app, grunts out to <packagename>.js */
var COUNCILVOTES        = {};
    COUNCILVOTES.config = {
        dataFile: 'WinnipegCouncilVotes.csv'
    }

var wardDetails   = {
    "Bowman":       { "Ward":" City", "Fullname":" Mayor Brian Bowman" },
    "Gillingham":   { "Ward":" St. James-Brooklands", "Fullname":" Scott Gillingham" },
    "Morantz":      { "Ward":" Charleswood-Tuxedo", "Fullname":" Marty Morantz" },
    "Lukes":        { "Ward":" St. Norbert", "Fullname":" Janice Lukes" },
    "Wyatt":        { "Ward":" Transcona", "Fullname":" Russ Wyatt" },
    "Orlikow":      { "Ward":" River Heights-Fort Garry", "Fullname":" John Orlikow" },
    "Allard":       { "Ward":" St. Boniface", "Fullname":" Matt Allard" },
    "Browaty":      { "Ward":" North Kildonan", "Fullname":" Jeff Browaty" },
    "Dobson":       { "Ward":" St. Charles", "Fullname":" Shawn Dobson" },
    "Eadie":        { "Ward":" Mynarski", "Fullname":" Ross Eadie" },
    "Gerbasi":      { "Ward":" Fort Rouge-East Fort Garry", "Fullname":" Jenny Gerbasi" },
    "Gilroy":       { "Ward":" Daniel McIntyre", "Fullname":" Cindy Gilroy" },
    "Mayes":        { "Ward":" St. Vital", "Fullname":" Brian Mayes" },
    "Pagtakhan":    { "Ward":" Point Douglas", "Fullname":" Mike Pagtakhan" },
    "Schreyer":     { "Ward":" Elmwood-East Kildonan", "Fullname":" Jason Schreyer" },
    "Sharma":       { "Ward":" Old Kildonan", "Fullname":" Devi Sharma"}
};

var webmillCacheURLBase     = '/webmill/news/interactives/data/',
    dataURL                 = webmillCacheURLBase + COUNCILVOTES.config.dataFile,
    htmlMarkup              = '',
    votesYes                = '',
    votesNo                 = '',
    votesAbstain            = '',
    voteCountYes            = 0,
    voteCountNo             = 0,
    voteCountAbstain        = 0,
    voteCountHTMLMarkup     = '',
    ds,    
    councilDetails,
    councilID,
    councilWard,
    councilVoteString,
    councilFullname,
    councilImage,
    voteCounter,
    voteCounterMax,    
    selectVoteOptions,
    optionSelected;

function init(){

    ds = new Miso.Dataset({
        url : dataURL,
        delimiter : ',',
    });

    ds.fetch({
        success: function(){
            voteData = ds.toJSON();
            processCouncilVotes(voteData);
        },
        error: function(err){
            if(window.console) {
                console.error('Error fetching data: ', err);
            }
        }
    });

}


function processCouncilVotes(voteData){

    var key,
        obj,
        voteKey,
        voteTitle;

    voteCounterMax  = Object.keys(voteData).length;
    voteCounter     = 1;

    for (key in voteData) {
        
        if (voteData.hasOwnProperty(key)) {
          
            obj = voteData[key];

            // Use the key (not vote_id) and vote_title to populate a pulldown
            voteKey     = 'voteKey-' + key; // obj['vote_id'];
            voteTitle   = obj['vote_title'];

            // Show the last vote as the default view
            if(voteCounter == voteCounterMax){
                optionSelected = 'selected="selected"';
                renderCouncilVotes(voteData, key);
            }
            else{
                optionSelected = ' ';    
            }

            selectVoteOptions += "\n" + '<option value="' + voteKey + '" ' + optionSelected + '>' + voteTitle + '</option>';

        }

        voteCounter++;                
        
    }

    $('#selectVote').append(selectVoteOptions);

}

function renderCouncilVotes(voteData, key){

    var obj = voteData[key];

    $('#voteWrapper').html('');                        
    $('#councilVoteDesc').html('');
    
    votesYes            = '',
    votesNo             = '',
    votesAbstain        = '',
    voteCountYes        = 0,
    voteCountNo         = 0,
    voteCountAbstain    = 0;

    for (var prop in obj) {
        
        if (obj.hasOwnProperty(prop)) {

            // console.log(prop + " = " + obj[prop]);
        
            var thisValue = obj[prop];

            // Handle each of the object properties accordingly
            if(prop == '_id'){
                // console.log('_id -- ignore');
            }
            else if(prop == 'vote_id'){
                voteID = prop
                var voteWrapperID   = 'vote-' + voteID;
            }
            else if(prop == 'vote_title'){
                // $('#councilVoteItem').html(thisValue);
            }
            else if(prop == 'vote_desc'){
                $('#councilVoteDesc').html(thisValue);
            }
            /* Convert date from timestamp -- THIS TIMESTAMP IS BS NEED TO GRAB FROM CSV AS STRING */
            else if(prop == 'vote_date'){
                // var timeString = timeConverter(thisValue);
                // $('#voteDate').html(thisValue);
            }
            else{
                /* This is a council member, their vote counts. */
                // console.log('prop: ' + prop);

                councilID       = prop;
                councilDetails  = wardDetails[councilID];

                // console.log("councilDetails: %o", councilDetails);

                if(councilDetails["Ward"] == 'city'){
                    councilWard = '';
                }
                else{
                    councilWard = councilDetails["Ward"];
                }

                councilFullname = councilDetails["Fullname"];
                councilImage    = councilID.toLowerCase() + '.png';
                councilVote     = thisValue;

                if(councilVote == 'Y'){
                    councilVoteString = 'Yes';
                }
                else if(councilVote == 'N'){
                    councilVoteString = 'No';
                }
                else{
                    councilVoteString = 'Abstain';
                }

                htmlMarkup      = ' '
                                + '<li class="councillor vote-' + councilVoteString + '" id="councillor-' + councilID + '">' + "\n"
                                + '     <div class="councillor-headshot"></div>'
                                + '     <div class="councillor-details">' + "\n"
                                + '         <b class="vote vote-' + councilVoteString + '">' + councilVoteString + '</b>' + "\n"
                                + '         <b class="fullname">' + councilFullname + '</b>' + "\n"
                                + '         <b class="ward">' + councilWard + '</b>' + "\n"
                                + '     </div>' + "\n"
                                + '</li>' + "\n\n";


                /* Drop it in the right bucket after it's clear out */
                if(councilVote == 'Y'){
                    votesYes            += htmlMarkup;
                    voteCountYes++;
                }
                else if(councilVote == 'N'){
                    votesNo             += htmlMarkup;
                    voteCountNo++;
                }
                else{
                    votesAbstain        += htmlMarkup;
                    voteCountAbstain++;
                }


            }

        }

    }
        
    $('#voteTallyY').html('');
    $('#voteTallyN').html('');
    $('#voteTallyA').html('');

    // Add a vote summary up top
    if(voteCountYes > 0 ){
        $('#voteTallyY').append('<b>Yes:</b> ' + voteCountYes);
    }
    else{
        $('#voteTallyY').append('<b>Yes:</b> -- ');
    }

    if(voteCountNo > 0 ){
        $('#voteTallyN').append('<b>No:</b> ' + voteCountNo);
    }
    else{
        $('#voteTallyN').append('<b>No:</b> -- ');
    }

    if(voteCountAbstain > 0 ){
        $('#voteTallyA').append('<b>Abstain:</b> ' + voteCountAbstain);
    }
    else{
        $('#voteTallyA').append('<b>Abstain:</b> --');
    }

    var voteListItems   = '' + "\n"
                        + '<ul class="small-block-grid-2 medium-block-grid-4 large-block-grid-4" id="council-votes">'        
                        + votesYes + "\n"
                        + votesNo + "\n"
                        + votesAbstain + "\n"
                        + '</ul>';

    $('#voteWrapper').html(voteListItems);

}


function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp*1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = month + ' ' + date + ', ' + year;
    return time;
}

// $('#selectVote').on('change',function(){
$(document.body).on('change', '#selectVote', function(){
    
    var selectedVoteID  = $(this).val();
    voteKey = selectedVoteID.replace('voteKey-', '');

    console.log("Changed the vote selector to " + selectedVoteID + ' & vote key is ' + voteKey );

    console.log("voteData[key] %o", voteData[voteKey]);

    renderCouncilVotes(voteData, voteKey);

});


// Need to add on change to teh pulldown.



init();
