council-votes
====================

Municipal vote tracker, content managed by a spreadsheet and displayed as a yay/nay grid. 

Council member details (full name, ward, image) are stroed in a JSON object within the JS. Moving to a meta worksheet is slated for next revision.




###Dev & Deployment###

1. Clone the repo.

2. Grab necessary vendor files used in this project.

	`bower install`

3. Grab the Grunt bit you'll need to work smarter. 

	`npm install`
	
4. Make and rename a copy of this Google Spreadsheet
	https://docs.google.com/a/cbc.ca/spreadsheet/ccc?key=0ApDmJDK9_O7IdG9FbG9JZ2NNRlN2UDlXcVhEUHhud2c#gid=0
	
5. Share the spreadsheet with editorial owner and make multimedia desk the owner. 

6. The first four columns are for context. A numerical ID, shortened title, vote description (full text), and date.

7. Remaining columns are one per council member using the last name as key. Last name corresponds to the wardDetails object in the base.src.js file. To be extracted in future version. Each vote is assigned a row, each council member gets a Y/N/A to record their vote.

8. Set up a webmill cache job to grab the data from GSS and store as localish CSV. Set the COUNCILVOTES.config.dataFile to the cache filename.

9. Upload to production, throw it into an iframe within Polopoly. 

10. Revel in the power of democratic memory. 


###To Do###
* move ward context to a second worksheet
* add fourth option to note members who are absent